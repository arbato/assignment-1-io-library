%define EXIT_CODE 60
%define WRITE_CODE 1
%define READ_CODE 0

section .text 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT_CODE 
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
	.loop:
		cmp byte[rdi+rax], 0
		je .done
		inc rax
		jmp .loop
	.done:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
    pop rsi 
    mov rdx, rax					; finds the length of the string and saves it in stack
    mov rax, WRITE_CODE				; places the pointer to the string in rsi
    mov rdi, 1				        ; places the length into rdx
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, WRITE_CODE
	mov rdx, 1
	mov rsi, rsp
	mov rdi, 1
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r10, rsp
    xor rcx, rcx			; set counter to zero
    xor r8, r8				; set divisor equal to zero
    mov r8, 10				; set divisor equal to 10
	mov rax, rdi			; place input in rax

   
    .loop:
        inc rcx
        xor rdx, rdx		; clear the remainder reg
        div r8
        add rdx, "0" 		; add '0' to convert to unicode char
        dec rsp
        mov byte[rsp], dl	; write the number byte-by-byte into stack

        test rax, rax 		; check if there is anything more left to divide

	jne .loop      
	
 	; prints the number
 	
    mov rsi, rsp

    mov rsp, r10 			; we have to restore the stack pointer

    mov rdx, rcx					
    mov rax, WRITE_CODE				; places the pointer to the string in rsi
    mov rdi, 1				; places the length into rdx
    syscall
   
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi 		; this instruction sets the sign flag

    jns .positive		; jump if not signed
    
    .negative: 			; negative case, add a minus and swich sign 
    push rdi
    mov rdi, "-"
    call print_char
    pop rdi
	neg rdi
	
    .positive:
    jmp print_uint		


string_equals: 			; inputs are in rdi and rsi
	xor rax, rax		; clear reg where letters will go
	xor rcx, rcx		; clear counter
    .loop:
    	mov al, byte[rcx+rdi]
    	cmp al, byte[rcx+rsi] 		; compare them letter-by-letter
    	jne .different
		inc rcx
		test rax, rax
		jnz .loop
		
	mov rax, 1
	ret

	.different:
		xor rax, rax
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ_CODE
	mov rdx, 1 			; just one char
	mov rdi, 0 			; stdin 
	push rax
	mov rsi, rsp
	syscall
    test rax, rax
    jl .error
	pop rax
    ret 

    .error:
        pop rax
        xor rax, rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
    
read_word:
	; rdi- addr, rsi - size
	; non-volatile regs!! volatile ones don't work

  	push r13		                ; beginning = r13
    mov r13, rdi										
    push r14		                ; end = r14
    mov r14, rdi
    push r15		                ; reverse counter = r15
    mov r15, rsi

    
    .leading_space: 				; ignore leading spaces
        call read_char
        cmp al, `\t`
        je .leading_space
        cmp al, `\n`
        je .leading_space
        cmp al, ` `
        je .leading_space

        
    .read_w: 	    						
    	test rax, rax               ; null-terminator?
    	je .success
        cmp al, `\t`
        je .success
        cmp al, `\n`
        je .success
        cmp al, ` `
        je .success
        
        dec r15 					; check if out of space
        test r15, r15
        jz .fail
        mov byte[r14], al 			; store char 
        inc r14
        call read_char
        jmp .read_w
        
    .success:
        mov byte[r14], 0 			; add null-terminator
        sub r14, r13				; get length
        mov rdx, r14
        mov rax, r13
        jmp .done
        
    .fail:
        xor rax, rax
        
    .done:
        pop r13
        pop r14
        pop r15
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint: 				
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        mov dl, byte[rdi + rcx]        
		cmp dl, "0"                     ; if less than zero, or bigger than 10, exit
        js .done
        cmp dl, ":"
        jns .done
        sub dl, "0"					    ; convert ascii to number

        imul rax, 10				    ; otherwise, multiply what is already read, and add new digit
        add rax, rdx
        inc rcx
        jmp .loop
    .done:
    	mov rdx, rcx
        ret
ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], "-" 	; im tired of commenting
    jne parse_uint 			
    cmp byte [rdi], "+"
    je .positive			; if it has a sign in front, rdi must be incremented
 							; otherwise, just use parse_uint
	.else: 					; just inverse and add one if the number is in "допкоd" format
	inc rdi
    call parse_uint
	neg rax
	inc rdx
	ret

	.positive:
	inc rdi
    call parse_uint
	ret
	
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: 			;rdi=string pointer, rsi=buffer pointer, rdx=buffer size 
    xor rax, rax			;clear counter
      .loop:
            cmp rdx, rax 					;compare buffer size with string length so far, stop if need be
            je .overflow 					; get the next char, put it in rcx
            mov cl, byte[rdi + rax]
            mov [rax+rsi], cl  				; Move char from rcx to memory. 
            inc rax							; mov [rdi + rax], [rax+rsi] does not work. so sad.
            cmp cl, 0 						; check for null terminator
            je .done
            jmp .loop
    
    .overflow:
        xor rax, rax       
    .done:  
        ret
